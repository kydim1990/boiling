# -*- coding: utf-8 -*-
from django.test import TestCase
from core.models import State, Order
from django.core.management import call_command
from django.contrib.auth.models import User
from datetime import datetime
import os


class StateTestCase(TestCase):
    def setUp(self):
        State.objects.create(code="Code1", name="Code1")

    def test_create(self):
        """State created"""
        state = State.objects.get(code="Code1")
        self.assertEqual(str(state), 'Code1')


class OrderTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='test', email='jacob@…', password='top_secret')
        fixdata = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'fixtures', 'initial_data.json')
        state = State.objects.create(code="Code1", name="Code1")
        print 'fixdata =%s' % fixdata
        call_command("loaddata", fixdata,              verbosity=0)
        self.date = datetime.today()
        Order.objects.create(user=self.user, state=state, count_players=5, summa=35, date=self.date)

    def test_create(self):
        """Order created """
        order = Order.objects.get(count_players=5, summa=35)
        self.assertFalse(order is None)


