function delete_story(){
    var id = $(this).attr('id');
    var id_story =id.split('_')[1];

    $.ajax({
      type: 'POST',
      url: '/delete_story/',
      data: {'id':id_story},
      success: function(){
        $('#' + id).parent().remove();
      }
    });
}

function add_like() {
    var elem = $(this);
    var id_story = elem.attr('alt');

    $.ajax({
      type: 'POST',
      url: '/story_add_like/',
      data: {'story_id':id_story},
      success: function(data){
        if (data['is_anon'])  {
            window.location.href = "/login/";
            return;
        }

          if (data['result']){
            var parent = elem.parent().parent();
            var count_likes = parent.find("span");
            var newvalue = parseInt(count_likes.text()) + 1;
            count_likes.text(newvalue);
          }

      }
    });
}

$( document ).ready(function() {
    $(".delete_story" ).click(delete_story);
    $(".likes").click(add_like);
});