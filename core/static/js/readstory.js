/**
 * Created by kydim on 05.09.2017.
 */

function delete_comment(){
     var id = $(this).attr('id');
     var id_comment =id.split('_')[1];

    $.ajax({
      type: 'POST',
      url: '/delete_comment/',
      data: {'id':id_comment},
      success: function(){
        $('#' + id).parent().remove();
      }
    });
}

function NewComment(){
    var text_val =  $("#text_newcomment").val();
    var story_id = $('#story').attr('data-id');

    $.ajax({
      type: 'POST',
      url: '/add_comment/',
      data: {'text': text_val, 'story_id': story_id},
      success: function(data){
        if (data['is_anon'])  {
            window.location.href = "/login/?next=/readstory/" + story_id + "/";
            return;
        }
        // console.log('success = ' + data );
        var newdiv = document.createElement("div");
        // newdiv.innerHTML = text_val + '  '+data['date'] ;
        newdiv.innerHTML = '<span class="data-text">' +  text_val   + '</span>';
        newdiv.innerHTML += '<span class="data-date">' +  data['date']   + '</span>';
        newdiv.innerHTML += '<a href="#" class="delete_comment" id="comment_' + data['id'] +'">Удалить</a>';
        $(newdiv).attr('class', "data-comment");
        $('#comments').append(newdiv);
        $("#text_newcomment").val('');
        $('#comment_' + data['id']).click(delete_comment);
      }
    });
}

$( document ).ready(function() {
    $(".delete_comment" ).click(delete_comment);
    $("#text_newcomment").on('change keyup paste', function() {
        var text = $("#text_newcomment");
        var add_comment_elem = $('#add_comment');
        if (!text.val()){
            add_comment_elem.prop('disabled', true);
        }
        else{
            // console.log('enabled');
            add_comment_elem.prop('disabled', false);
        }

    });
});