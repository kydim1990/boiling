# -*- coding: utf-8 -*-
from django.forms import CharField, Form, PasswordInput, ValidationError, DateField, \
    IntegerField, DateTimeField, SplitDateTimeWidget, DateTimeInput, Textarea, TextInput
from django.contrib.auth import authenticate
from django.core import validators
from django.contrib.auth.models import User
from django.contrib.auth import login as auth_login, update_session_auth_hash, \
    logout as auth_logout
from .models import Story, Comment


class RegistrationForm(Form):
    default_validators = [validators.validate_slug]
    username = CharField(label='Имя', max_length=100)
    lastname = CharField(label='Фамилия', max_length=100)
    email = CharField(label='Email', max_length=100)
    password = CharField(label='Пароль', max_length=100, widget=PasswordInput())

    def clean(self):
        username = self.cleaned_data.get('username')
        lastname = self.cleaned_data.get('lastname')
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        if User.objects.filter(username=username).exists():
            raise ValidationError("User with name %s is exists" % username)
        return self.cleaned_data

    def register(self, request):
        username = self.cleaned_data.get('username')
        lastname = self.cleaned_data.get('lastname')
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        user = User.objects.create_user(username, email, password)
        user.last_name = lastname
        user.set_password(password)
        user.save()
        auth_login(request, user)


class LoginForm(Form):
    username = CharField(label='Логин', max_length=100)
    password = CharField(label='Пароль', max_length=100, widget=PasswordInput())

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        if not user or not user.is_active:
            raise ValidationError("Sorry, that login was invalid. Please try again.")
        return self.cleaned_data

    def login(self, request):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        return user


class ChangePasswordForm(Form):
    old_password = CharField(label='Старый пароль', max_length=100, widget=PasswordInput())
    new_password = CharField(label='Старый пароль', max_length=100, widget=PasswordInput())

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(ChangePasswordForm, self).__init__(*args, **kwargs)

    def clean(self):
        print self.user
        old_password = self.cleaned_data.get('old_password')
        print old_password, self.user.check_password(old_password)
        new_password = self.cleaned_data.get('new_password')
        if not self.user.check_password(old_password):
            raise ValidationError("old password is wrong")
        if old_password == new_password:
            raise ValidationError("New password must be different old password")
        return self.cleaned_data

    def change_password(self, request):
        new_password = self.cleaned_data.get('new_password')
        request.user.set_password(new_password)
        request.user.save()
        update_session_auth_hash(request, request.user)


class CommentForm(Form):
    text = CharField(label='Add comment', max_length=500, widget=Textarea)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.story = kwargs.pop('story', None)
        super(CommentForm, self).__init__(*args, **kwargs)

    def clean(self):
        # if Order.objects.filter(date=date).exists():
        #     raise ValidationError("Order with name %s is exists" % date)
        return self.cleaned_data

    def save(self, request):
        self.text = self.cleaned_data.get('text')
        comment = Comment.objects.create(text=self.text, story=self.story, user=self.user)
        comment.save()


class StoryForm(Form):
    text = CharField(label="", max_length=5000, widget=Textarea)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(StoryForm, self).__init__(*args, **kwargs)

    def clean(self):
        # count_players = self.cleaned_data.get('count_players')
        # print self.story
        # if Order.objects.filter(date=date).exists():
        #     raise ValidationError("Order with name %s is exists" % date)
        return self.cleaned_data

    def save(self, request):
        text = self.cleaned_data.get('text')
        newstory = Story.objects.create(text=text, user=self.user)
        newstory.save()

