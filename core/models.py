# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from datetime import datetime


@python_2_unicode_compatible
class Story(models.Model):
    date = models.DateTimeField('date published', default=datetime.today)
    text = models.TextField('Text', max_length=5000, blank=True)
    user = models.ForeignKey(User, related_name='User')
    state = models.TextField('State', max_length=50, default='CREATED')
    likes = models.ManyToManyField(User, related_name='User_likes')

    def delete(self, using=None, keep_parents=False):
        self.state = 'DELETED'
        self.save()

    @property
    def getshorttext(self):
        return self.text[:300]+'...'

    @property
    def count_comment(self):
        return Comment.objects.filter(story=self).count()

    def __str__(self):
        likes = ' likes: ' + str(self.likes.count()) + ' '
        return '%s_%s' % (self.text[:20] + likes + '...', self.date.strftime('%b %d %Y %H:%M:%S'))

    def add_like(self, user):
        if self.likes.filter(pk=user.pk).exists():
            self.likes.add(user)
            self.save()


@python_2_unicode_compatible
class Comment(models.Model):
    date = models.DateTimeField('date published', default=datetime.today)
    text = models.TextField('Text', max_length=500)
    story = models.ForeignKey(Story, related_name='Story')
    state = models.TextField('State', max_length=50, default='CREATED')
    user = models.ForeignKey(User, related_name='UserComment', null=True)

    def delete(self, using=None, keep_parents=False):
        self.state = 'DELETED'
        self.save()

    def __str__(self):
        return self.text

