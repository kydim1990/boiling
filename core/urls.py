from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.conf.urls import include

from . import views

urlpatterns = [
    url(r'^login/$', views.login, name='login'),
    url(r'^change_pass/$', views.change_pass, name='change_pass'),
    url(r'^registration/$', views.registration, name='registration'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^index/$', views.index, name='index'),
    # -- reset password
    url(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
    # --test
    url(r'test/$', views.test, name='test'),
    # --story
    url(r'^readstory/(?P<id>[0-9]+)/$', views.readstory, name='readstory'),
    url(r'^addstory/$', views.addstory, name='addstory'),
    url(r'^delete_comment/$', views.delete_comment, name='delete_comment'),
    url(r'^delete_story/$', views.delete_story, name='delete_story'),
    url(r'^add_comment/$', views.add_comment, name='add_comment'),
    url(r'^story_add_like/$', views.story_add_like, name='story_add_like'),
    #
    url(r'^about_as/', include('cms.urls'), name='about_us'),
    url(r'', views.index, name='index'),
    # url(r'', include('cms.urls'), name='index'),
]
