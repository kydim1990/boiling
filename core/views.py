from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .forms import RegistrationForm, LoginForm, ChangePasswordForm, CommentForm, StoryForm
from django.shortcuts import render, redirect
from django.contrib.auth import login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from models import Story, Comment
from datetime import datetime
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


@csrf_exempt
def index(request):
    allstoryies = Story.objects.filter(state='CREATED').order_by('id').reverse()
    paginator = Paginator(allstoryies, 9)
    page = request.GET.get('page')
    try:
        stories = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        stories = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        stories = paginator.page(paginator.num_pages)
    return render(request, 'core/index.html', {'stories': stories})


@csrf_exempt
def logout(request):
    auth_logout(request)
    return redirect(index)


@csrf_exempt
def login(request):
    form = LoginForm(request.POST or None)
    if request.POST and form.is_valid():
        user = form.login(request)
        if user:
            auth_login(request, user)
            if request.GET.get('next') is None:
                return redirect(index)
            else:
                return redirect(request.GET.get('next'))
    return render(request, 'core/login.html', {'form': form})


@csrf_exempt
def registration(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.register(request)
            return redirect(index)
        else:
            return render(request, 'core/registration.html', {'form': form})
    else:
        form = RegistrationForm()
        return render(request, 'core/registration.html', {'form': form})


@login_required
@csrf_exempt
def change_pass(request):
    if request.method == 'POST':
        form = ChangePasswordForm(request.POST, user=request.user)
        if form.is_valid():
            form.change_password(request)
            messages.success(request, 'Password success changed')
            return render(request, 'core/change_password.html', {'form': form})
        else:
            return render(request, 'core/change_password.html', {'form': form})
    else:
        form = ChangePasswordForm()
        return render(request, 'core/change_password.html', {'form': form})


def test(request):
    from django.core.mail import EmailMessage

    email = EmailMessage('title', 'body', to=['kydim1312@yandex.ru', 'kydimtest@gmail.com'])
    res = email.send()
    return JsonResponse({'test': res})


@csrf_exempt
def readstory(request, id):
    story = Story.objects.get(id=id)
    comments = Comment.objects.filter(story=story, state='CREATED')
    if request.method == 'POST':
        form = CommentForm(request.POST, user=request.user, story=story)
        if form.is_valid():
            form.save(request)
            return render(request, 'core/story.html', {'story': story, 'comments': comments,
                                                       'form': CommentForm()})
        else:
            return JsonResponse({'story': 'test'})
    else:
        form = CommentForm()
        return render(request, 'core/story.html', {'story': story, 'comments': comments,
                                                   'form': form})


@login_required
@csrf_exempt
def addstory(request):
    if request.method == 'POST':
        form = StoryForm(request.POST, user=request.user)
        if form.is_valid():
            form.save(request)
            return redirect('/index/')
        else:
            form = StoryForm()
            return render(request, 'core/addstory.html', {'form': form})
    else:
        form = StoryForm()
        return render(request, 'core/addstory.html', {'form': form})


@csrf_exempt
def delete_story(request):
    story_del = Story.objects.get(id=request.POST.get('id'))
    story_del.delete()
    return JsonResponse({'status': 'ok'})


@csrf_exempt
def delete_comment(request):
    comment_del = Comment.objects.get(id=request.POST.get('id'))
    comment_del.delete()
    return JsonResponse({'status': 'ok'})


@csrf_exempt
def add_comment(request):
    if request.user.is_anonymous():
        return JsonResponse({'is_anon': True})
    story = Story.objects.get(id=request.POST.get('story_id'))
    user = User.objects.get(id=request.user.id)
    newcomment = Comment.objects.create(story=story, text=request.POST.get('text'),  user=user)
    newcomment.save()
    return JsonResponse({'id': newcomment.id,
                         'date': datetime.strftime(newcomment.date, '%d-%m-%y %H:%M')})


@csrf_exempt
def story_add_like(request):
    if request.method != 'POST' or request.user.is_anonymous():
        return JsonResponse({'result': False, 'is_anon': True})
    story = Story.objects.get(id=request.POST.get('story_id'))
    user = User.objects.get(id=request.user.id)
    if not story.likes.filter(pk=user.id).exists():
        story.likes.add(user)
        story.save()
        return JsonResponse({'result': True})
    else:
        return JsonResponse({'result': False})
